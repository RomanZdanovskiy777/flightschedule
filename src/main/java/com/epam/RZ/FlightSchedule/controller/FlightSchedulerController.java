package com.epam.RZ.FlightSchedule.controller;

import com.epam.RZ.FlightSchedule.DynamoDB.model.Flight;
import com.epam.RZ.FlightSchedule.Service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Class with defined controllers for frontend
 */
@RestController
@Validated
public class FlightSchedulerController {

    @Autowired
    private FlightService flightService;

    /**
     * Controller for add flight
     * @param flightID Flight ID. Must be integer greater than or equal to 1
     * @param departureID Departure ID. Must be integer greater than or equal to 1
     * @param arrivalID Arrival ID. Must be integer greater than or equal to 1
     * @param date Date in ISO8601 format. Should be in ISO8601 format YYYY-MM-DDTHH:MMZ
     * @return Flight object added to DB in json format
     * @exception ConstraintViolationException if parameters validation did not passed
     */
    @GetMapping("addFlight/{flightID}/{departureID}/{arrivalID}/{date}")
    public Flight addFlight(
            @PathVariable @Min(1) Long flightID,
            @PathVariable @Min(1) Long departureID,
            @PathVariable @Min(1) Long arrivalID,
            @PathVariable @Pattern(regexp = "[0-9]{1,4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-6][0-9]Z") String date
    ) {
        return flightService.createFlight(new Flight(flightID, departureID, arrivalID, date));
    }

    /**
     * Controller for update flight
     * @param flightID Flight ID. Must be integer greater than or equal to 1
     * @param departureID Departure ID. Must be integer greater than or equal to 1
     * @param arrivalID Arrival ID. Must be integer greater than or equal to 1
     * @param date Date in ISO8601 format. Must be in ISO8601 format YYYY-MM-DDTHH:MMZ
     * @return Flight object updated in DB in json format
     * @exception ConstraintViolationException if parameters validation did not passed
     */
    @GetMapping("updateFlight/{flightID}/{departureID}/{arrivalID}/{date}")
    public Flight updateFlight(
            @PathVariable @Min(1) Long flightID,
            @PathVariable @Min(1) Long departureID,
            @PathVariable @Min(1) Long arrivalID,
            @PathVariable @Pattern(regexp = "[0-9]{1,4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-6][0-9]Z") String date
    ) {
        return flightService.updateFlight(new Flight(flightID, departureID, arrivalID, date));
    }

    /**
     * Controller for update flight
     * @param flightID Flight ID to delete. Must be integer greater than or equal to 1
     * @return String with successful massage if deleted in DB
     * @exception ConstraintViolationException if parameters validation did not passed
     */
    @GetMapping("deleteFlight/{flightID}")
    public String deleteFlight(@PathVariable @Min(1) Long flightID) {

        return flightService.deleteFlight(flightID);
    }

    /**
     * Controller for searching flight by arrival id
     * @param arrivalID Arrival ID to search. Must be integer greater than or equal to 1
     * @return ArrayList of found flights in json format
     * @exception ConstraintViolationException if parameters validation did not passed
     */
    @GetMapping("arrivalID/{arrivalID}")
    ArrayList<Optional<Flight>> searchingByArrivalID(@PathVariable @Min(1) Long arrivalID) {

        return flightService.findAllByArrivalID(arrivalID);
    }

    /**
     * Controller for searching flight by arrival id
     * @param date Date to search. Must be in ISO8601 format YYYY-MM-DDTHH:MMZ
     * @return ArrayList of found flights in json format
     * @exception ConstraintViolationException if parameters validation did not passed
     */
    @GetMapping("date/{date}")
    ArrayList<Optional<Flight>> searchingByDate(
            @PathVariable @Pattern(regexp = "[0-9]{1,4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-6][0-9]Z") String date
    ) {
        return flightService.findAllByDate(date);
    }

    /**
     * Controller for searching flight by arrival id
     * @param departureID Departure ID to search. Must be integer greater than or equal to 1
     * @return ArrayList of found flights in json format
     * @exception ConstraintViolationException if parameters validation did not passed
     */
    @GetMapping("departureID/{departureID}")
    ArrayList<Optional<Flight>> searchingByDepartureID(@PathVariable @Min(1) Long departureID) {
        return flightService.findAllByDepartureID(departureID);
    }

    /**
     * Controller for searching flight by arrival id
     * @param flightID Flight ID to search. Must be integer greater than or equal to 1
     * @return Flight object found by ID in json format
     * @exception ConstraintViolationException if parameters validation did not passed
     */
    @GetMapping("flightID/{flightID}")
    Optional<Flight> searchingByFlightID(@PathVariable @Min(1) Long flightID) {
        return flightService.findByFlightID(flightID);
    }
}
