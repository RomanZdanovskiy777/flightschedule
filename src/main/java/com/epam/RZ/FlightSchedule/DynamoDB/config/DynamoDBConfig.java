package com.epam.RZ.FlightSchedule.DynamoDB.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * DynamoDB connection configuration
 */
@Configuration
@EnableDynamoDBRepositories(basePackages = "com.epam.RZ.FlightSchedule.DynamoDB.repository")
public class DynamoDBConfig {

    /**
     * Amazon dynamodb endpoint. Read from application.properties
     */
    @Value("${amazon.dynamodb.endpoint}")
    private String amazonDynamoDBEndpoint;

    /**
     * Amazon AWS access key. Read from application.properties
     */
    @Value("${amazon.aws.accesskey}")
    private String amazonAwsAccessKey;

    /**
     * Amazon AWS secret key. Read from application.properties
     */
    @Value("${amazon.aws.secretkey}")
    private String amazonAwsSecretKey;

    /**
     * Amazon AWS region. Read from application.properties
     */
    @Value("${amazon.aws.region}")
    private String amazonAwsRegion;

    /**
     * Bean with DynamoDB mapper
     * @return DynamoDBMapper
     */
    @Bean
    @Primary
    public DynamoDBMapper dynamoDBMapper() {
        return new DynamoDBMapper(amazonDynamoDB());
    }

    /**
     * Specify dynamoDB connection
     * @return AmazonDynamoDB for DynamoDB mapper
     */
    @Bean
    public AmazonDynamoDB amazonDynamoDB() {
        return AmazonDynamoDBClientBuilder.standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(amazonDynamoDBEndpoint, amazonAwsRegion))
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(amazonAwsAccessKey, amazonAwsSecretKey)))
                .build();
    }
}
