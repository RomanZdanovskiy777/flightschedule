package com.epam.RZ.FlightSchedule.DynamoDB.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * Class to define model storage in DynamoDB
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@DynamoDBTable(tableName = "flights")
public class Flight implements Serializable {

    /**
     * Primary key to storage in DynamoDB
     */
    @DynamoDBHashKey(attributeName = "flightID")
    private Long flightID;

    /**
     * Departure ID key
     */
    @DynamoDBAttribute
    private Long departureID;

    /**
     * Arrival ID key
     */
    @DynamoDBAttribute
    private Long arrivalID;

    /**
     * Date of departure key. Should be in ISO8601 format YYYY-MM-DDTHH:MMZ
     */
    @DynamoDBAttribute
    private String date;

}
