package com.epam.RZ.FlightSchedule.DynamoDB.repository;

import com.epam.RZ.FlightSchedule.DynamoDB.model.Flight;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Interface to work with flight model in DynamoDB
 */
@EnableScan
public interface FlightRepository extends CrudRepository<Flight, Long> {

    ArrayList<Optional<Flight>> findAllByDepartureID(Long departureID);

    ArrayList<Optional<Flight>> findAllByArrivalID(Long arrivalID);

    ArrayList<Optional<Flight>> findAllByDate(String date);

}
