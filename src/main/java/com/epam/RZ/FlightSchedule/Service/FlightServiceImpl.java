package com.epam.RZ.FlightSchedule.Service;

import com.epam.RZ.FlightSchedule.DynamoDB.model.Flight;
import com.epam.RZ.FlightSchedule.DynamoDB.repository.FlightRepository;
import com.epam.RZ.FlightSchedule.exception.FlightAlreadyExistException;
import com.epam.RZ.FlightSchedule.exception.FlightNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.NonTransientDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Flight service interface implementation for DynamoDB DB
 */
@Service
public class FlightServiceImpl implements FlightService {

    /**
     * Flight repository injection for DynamoDB DB
     */
    @Autowired
    private FlightRepository flightRepository;

    /**
     * Creates Flight in DB
     * @param flight Object to create in DB
     * @return Object created in DB
     * @exception FlightAlreadyExistException in case if object to create in DB already exist in DB
     */
    @Override
    public Flight createFlight(Flight flight) {
        if (!flightRepository.existsById(flight.getFlightID())) return flightRepository.save(flight);
        else throw new FlightAlreadyExistException(flight.getFlightID());
    }

    /**
     * Finds Flight by passed ID in DB
     * @param id ID of the flight to find in DB
     * @return Flight from DB
     * @exception FlightNotFoundException in case no Flight in DB with such ID
     */
    @Override
    public Optional<Flight> findByFlightID(Long id) {
        return Optional.ofNullable(flightRepository.findById(id).orElseThrow(() -> new FlightNotFoundException("flightID", id)));
    }

    /**
     * Finds all Flight Objects with passed arrivalID in DB
     * @param id Arrival ID to find in DB
     * @return ArrayList of Flight objects with passed arrivalID
     * @exception FlightNotFoundException in case no Flight in DB with such arrivalID
     */
    @Override
    public ArrayList<Optional<Flight>> findAllByArrivalID(Long id) {

        ArrayList<Optional<Flight>> response = flightRepository.findAllByArrivalID(id);
        if (response.isEmpty()) {
            throw new FlightNotFoundException("arrivalID", id);
        } else return response;
    }

    /**
     * Finds all Flight Objects with passed departureID in DB
     * @param id Departure ID to find in DB
     * @return ArrayList of Flight objects with passed departureID
     * @exception FlightNotFoundException in case no Flight in DB with such departureID
     */
    @Override
    public ArrayList<Optional<Flight>> findAllByDepartureID(Long id) {

        ArrayList<Optional<Flight>> response = flightRepository.findAllByDepartureID(id);
        if (response.isEmpty()) {
            throw new FlightNotFoundException("departureID", id);
        } else return response;
    }

    /**
     * Finds all Flight Objects with passed date in DB
     * @param date Date to find in DB
     * @return ArrayList of Flight objects with passed date
     * @exception FlightNotFoundException in case no Flight in DB with such date
     */
    @Override
    public ArrayList<Optional<Flight>> findAllByDate(String date) {

        ArrayList<Optional<Flight>> response = flightRepository.findAllByDate(date);
        if (response.isEmpty()) {
            throw new FlightNotFoundException("date", date);
        } else return response;
    }

    /**
     * Deletes Flight with passed Flight ID in DB
     * @param id Flight ID to delete such flight in DB
     * @return String to confirm successful delete in DB
     * @exception FlightNotFoundException in case no Flight in DB with such id
     */
    @Override
    public String deleteFlight(Long id) {
        try {
            flightRepository.deleteById(id);
        } catch (NonTransientDataAccessException e) {
            throw new FlightNotFoundException("flightID", id);
        }
        return "Flight successfully deleted";
    }

    /**
     * Deletes Flight with passed Flight ID in DB
     * @param flight Flight object to update in DB
     * @return Flight object that was updated successful
     * @exception FlightNotFoundException in case no Flight in DB with such id
     */
    @Override
    public Flight updateFlight(Flight flight) {
        if(flightRepository.existsById(flight.getFlightID())) return flightRepository.save(flight);
        else throw new FlightNotFoundException("FlightID", flight.getFlightID());
    }

    /**
     * Checks DB for Flight with passed ID
     * @param id Flight ID to check in DB
     * @return Boolean true if exist, false if not exist
     */
    @Override
    public boolean existsById(Long id) {
        return flightRepository.existsById(id);
    }
}
