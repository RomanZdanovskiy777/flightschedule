package com.epam.RZ.FlightSchedule.Service;

import com.epam.RZ.FlightSchedule.DynamoDB.model.Flight;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Declaration of all actions supporting with Flight objects in DB
 */
public interface FlightService {

    Flight createFlight(Flight flight);

    Optional<Flight> findByFlightID(Long id);

    ArrayList<Optional<Flight>> findAllByArrivalID(Long id);

    ArrayList<Optional<Flight>> findAllByDepartureID(Long id);

    ArrayList<Optional<Flight>> findAllByDate(String date);

    String deleteFlight(Long id);

    Flight updateFlight(Flight flight);

    boolean existsById(Long id);
}
