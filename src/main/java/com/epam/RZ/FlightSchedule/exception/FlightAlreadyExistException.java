package com.epam.RZ.FlightSchedule.exception;

/**
 * Class to define exception when flight already exist
 */
public class FlightAlreadyExistException extends RuntimeException {
    public FlightAlreadyExistException(Object id) {
        super("Flight with id = " + id + " already exist");
    }
}
