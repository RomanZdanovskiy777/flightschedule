package com.epam.RZ.FlightSchedule.exception;

/**
 * Class to define exception when flight did not found in DB
 */
public class FlightNotFoundException extends RuntimeException {
    public FlightNotFoundException(String parameterType, Object id) {
        super("No Flight found with " + parameterType + " parameter = " + id);
    }
}
