package com.epam.RZ.FlightSchedule.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * Exception handler class to define response to the frontend in the exceptional situations
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Exception handler for FlightNotFoundException
     * @param ex Exception that was happened when program executed
     * @param request Request that bring to the Exception
     * @return Response to the front end
     */
    @ExceptionHandler(FlightNotFoundException.class)
    protected ResponseEntity<Object> handleFlightNotFoundEx(FlightNotFoundException ex, WebRequest request) {
        ApiError apiError = new ApiError("No such flight", ex.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }

    /**
     * Exception handler for FlightAlreadyExistException
     * @param ex Exception that was happened when program executed
     * @param request Request that bring to the Exception
     * @return Response to the front end
     */
    @ExceptionHandler(FlightAlreadyExistException.class)
    protected ResponseEntity<Object> handleFlightAlreadyExistEx(FlightAlreadyExistException ex, WebRequest request) {
        ApiError apiError = new ApiError("Flight already exist", ex.getMessage());
        return new ResponseEntity<>(apiError, BAD_REQUEST);
    }

    /**
     * Exception handler for MethodArgumentTypeMismatchException
     * @param ex Exception that was happened when program executed
     * @param request Request that bring to the Exception
     * @return Response to the front end
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                      WebRequest request) {
        ApiError apiError = new ApiError();
        apiError.setMessage(String.format("The parameter '%s' of value '%s' could not be converted to type '%s'",
                ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName()));
        apiError.setDebugMessage(ex.getMessage());
        return new ResponseEntity<>(apiError, BAD_REQUEST);
    }

    /**
     * Exception handler for ConstraintViolationException
     * @param ex Exception that was happened when program executed
     * @return Response to the front end
     */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException ex) {
        return new ResponseEntity<>("Not valid due to validation error: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Exception handler for all undescribed exceptions
     * @param ex Exception that was happened when program executed
     * @param request Request that bring to the Exception
     * @return Response to the front end
     */
    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        ApiError apiError = new ApiError();
        apiError.setMessage("Internal Exception. Call the administrator.");
        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Exception handler if request do not support by the server
     * @param ex Exception that was happened when program executed
     * @param headers Request header
     * @param status Request status
     * @param request Request that bring to the Exception
     * @return Response to the front end
     */
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
                                                                   HttpStatus status, WebRequest request) {
        return new ResponseEntity<Object>(new ApiError("No Handler Found", ex.getMessage()), status);
    }


}
