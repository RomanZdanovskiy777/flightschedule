package com.epam.RZ.FlightSchedule;

import com.epam.RZ.FlightSchedule.DynamoDB.model.Flight;
import com.epam.RZ.FlightSchedule.DynamoDB.repository.FlightRepository;
import com.epam.RZ.FlightSchedule.controller.FlightSchedulerController;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.stream.Stream;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class FlightSchedulerControllerSearchingByDateParameterizedTest {

    @Autowired
    private FlightSchedulerController flightSchedulerController;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FlightRepository flightRepository;

    static Stream<Arguments> paramTestDataProvider() {

        return Stream.of(
                arguments(
                        true,
                        true,
                        status().isOk(),
                        "{\"flightID\":10001,\"departureID\":10002,\"arrivalID\":10003,\"date\":\"2021-06-16T16:49Z\"}",
                        "application/json",
                        "/date/2021-06-16T16:49Z"
                ),
                arguments(
                        false,
                        false,
                        status().isBadRequest(),
                        "Not valid due to validation error",
                        "text/plain;charset=UTF-8",
                        "/date/2"
                ),
                arguments(
                        false,
                        false,
                        status().isNotFound(),
                        "No Handler Found",
                        "application/json",
                        "/date/"
                ),
                arguments(
                        false,
                        false,
                        status().isNotFound(),
                        "No such flight",
                        "application/json",
                        "/date/2021-06-16T16:49Z"
                )
        );
    }

    @ParameterizedTest
    @MethodSource("paramTestDataProvider")
    public void searchingByDateParameterizedTest(Boolean needToCreate, Boolean needToDelete, ResultMatcher status,
                                           String content, String contentType, String request) throws Exception {

        if (needToCreate)
            flightRepository.save(new Flight(10001L, 10002L, 10003L, "2021-06-16T16:49Z"));

        this.mockMvc.perform(get(request))
                .andDo(print())
                .andExpect(status)
                .andExpect(content().contentType(contentType))
                .andExpect(content().string(containsString(content)));

        if (needToDelete) flightRepository.deleteById(10001L);
    }


}
